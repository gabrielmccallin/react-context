import React, { createContext, useReducer } from "react";
import { NoPropsForMe } from "./context-example/NoPropsForMe";
import { ShowState } from "./context-example/ShowState";

export const Context = createContext({
  state: [] as string[],
  dispatch: (state: string) => {},
});

export const App = () => {
  const reducer = (state: string[], newData: string) => {
    return [...state, newData];
  };

  const [state, dispatch] = useReducer(reducer, ["🥇"]);

  return (
    <div>
      <Context.Provider value={{ state, dispatch }}>
        <NoPropsForMe />
        <ShowState />
      </Context.Provider>
    </div>
  );
};
