import { useContext } from "react";
import { Context } from "../App";

export const ShowState = () => {
  const { state } = useContext(Context);
  return <div>{state?.toString()}</div>;
};
