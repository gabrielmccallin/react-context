export const Clicker = ({
  title,
  updateCount,
}: {
  title: string;
  updateCount: (event: any) => void;
}) => {
  return <button onClick={updateCount}>{title}</button>;
};
