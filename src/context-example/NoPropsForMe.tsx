import React, { useContext } from "react";
import { Clicker } from "./Clicker";
import { Context } from "../App";

export const NoPropsForMe: React.FC = () => {
  const { dispatch } = useContext(Context);
  const handleClick = () => {
    dispatch(" 😀");
  };
  return <Clicker updateCount={handleClick} title="click me" />;
};
